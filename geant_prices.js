import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));

const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");

const init = async () => {
  const decoder = new TextDecoder();

  let client = new FTPClient(ftpHost, {
    user: user,
    pass: pass,
    mode: "passive",
    port: 21,
  });

  await client.connect();
  let price_data = await downloadFile(client, "./wg-prec-sagal.csv");
  if (price_data) {
    let price_data_text = decoder.decode(price_data).split("\n");
    let payloads = [];
    for (let line of price_data_text) {
      try {
        if (line) {
          let fields = line.split(",");
          let sku = fields[0].trim();
          let price_1 = fields[1].trim();
          let price_2 = fields[2].trim();
          let price =
            price_1 == price_2
              ? { price: Number(price_2), currency: "$" }
              : { price: Number(price_2), currency: "U$S" };
          let payload = {
            sku: sku,
            client_id: clientId,
            integration_id: clientIntegrationId,
            options: {
              merge: false,
            },
            ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
              return {
                ecommerce_id: ecommerce_id,
                variants: [],
                properties: [
                  {
                    price: {
                      value: price.price,
                      currency: price.currency,
                    },
                  },
                ],
              };
            }),
          };
          payloads.push(payload);
        }
      } catch (e) {
        console.log(`Could not send article: ${e.message}`);
      }
    }
    await sagalDispatchBatch({
      products: payloads,
      batch_size: 100,
    });
    await renameCsvFileWithTimestamp(client, "./wg-prec-sagal.csv");
  } else {
    console.log(`Price data file does not exist`);
  }
  await client.close();
};

async function downloadFile(client, file) {
  try {
    let data = await client.download(file);
    return data;
  } catch (e) {
    return null;
  }
}

async function renameCsvFileWithTimestamp(client, file) {
  try {
    await client.rename(file, file.replace(".csv", `${Date.now()}.csv`));
  } catch (e) {
    console.log(`Failed to rename file`);
  }
}

await init();
